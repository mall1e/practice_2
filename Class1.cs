﻿using System;
using System.Collections.Generic;

namespace GraphicDesignSoftware
{
    // Abstract prototype class
    public abstract class GraphicPrototype
    {
        public abstract GraphicPrototype Clone();
    }

    // Concrete prototype class
    public class GraphicElement : GraphicPrototype
    {
        private List<GraphicPrimitive> _primitives = new List<GraphicPrimitive>();

        public void AddPrimitive(GraphicPrimitive primitive)
        {
            _primitives.Add(primitive);
        }

        public override GraphicPrototype Clone()
        {
            GraphicElement clone = new GraphicElement();
            foreach (GraphicPrimitive primitive in _primitives)
            {
                clone.AddPrimitive((GraphicPrimitive)primitive.Clone());
            }
            return clone;
        }

        public void Draw()
        {
            Console.WriteLine("Drawing graphic element:");
            foreach (GraphicPrimitive primitive in _primitives)
            {
                primitive.Draw();
            }
            Console.WriteLine();
        }
    }

    // Linker class
    public static class Linker
    {
        public static GraphicElement Link(params GraphicPrimitive[] primitives)
        {
            GraphicElement element = new GraphicElement();
            foreach (GraphicPrimitive primitive in primitives)
            {
                element.AddPrimitive(primitive);
            }
            return element;
        }
    }

    // Abstract graphic primitive class
    public abstract class GraphicPrimitive : GraphicPrototype
    {
        protected int _width;
        protected int _height;
        protected ConsoleColor _color;

        public GraphicPrimitive(int width, int height, ConsoleColor color)
        {
            _width = width;
            _height = height;
            _color = color;
        }

        public override GraphicPrototype Clone()
        {
            return (GraphicPrimitive)this.MemberwiseClone();
        }

        public abstract void Draw();
    }

    // Concrete graphic primitive classes
    public class Line : GraphicPrimitive
    {
        public Line(int width, int height, ConsoleColor color) : base(width, height, color) { }

        public override void Draw()
        {
            Console.ForegroundColor = _color;
            Console.WriteLine($"Drawing line with width {_width} and height {_height}");
            Console.ResetColor();
        }
    }

    public class Circle : GraphicPrimitive
    {
        public Circle(int width, int height, ConsoleColor color) : base(width, height, color) { }

        public override void Draw()
        {
            Console.ForegroundColor = _color;
            Console.WriteLine($"Drawing circle with width {_width} and height {_height}");
            Console.ResetColor();
        }
    }

    public class Rectangle : GraphicPrimitive
    {
        public Rectangle(int width, int height, ConsoleColor color) : base(width, height, color) { }

        public override void Draw()
        {
            Console.ForegroundColor = _color;
            Console.WriteLine($"Drawing rectangle with width {_width} and height {_height}");
            Console.ResetColor();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            // Create some graphic primitives
            Line line = new Line(10, 2, ConsoleColor.Red);
            Circle circle = new Circle(5, 5, ConsoleColor.Green);
            Rectangle rectangle = new Rectangle(8, 4, ConsoleColor.Blue);

            // Link the primitives together to create a custom graphic element
            GraphicElement customElement = Linker.Link(line, circle, rectangle);

            // Clone the custom graphic element to create a new one
            GraphicElement clonedElement = (GraphicElement)customElement.Clone();

            // Draw the custom element and the cloned element
            customElement.Draw();
            clonedElement.Draw();

            Console.ReadKey();
        }
    }
}
