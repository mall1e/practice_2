```plantuml

@startuml

class Rectangle {
+ Draw() 
- _width: int       
- _height: int      
- _color: ConsoleColor
}

class Line {
+ Draw() 
- _width: int       
- _height: int      
- _color: ConsoleColor
}

class Circle {
+ Draw() 
- _width: int       
- _height: int      
- _color: ConsoleColor
}

class GraphicPrimitive {
+ Draw() 
+ Clone()
- _width: int       
- _height: int      
- _color: ConsoleColor
}

class Linker {
+ Link(primitives: GraphicPrimitive[]) 
}

class GraphicElement {
- _primitives: List<GraphicPrimitive> 
+ Draw()
+ AddPrimitive(primitive: GraphicPrimitive) 
}

class GraphicPrototype {
+ Clone() 
}

GraphicPrimitive <|-- Rectangle
GraphicPrimitive <|-- Line
GraphicPrimitive <|-- Circle
Linker <|-- GraphicPrimitive
GraphicElement <|-- Linker
GraphicElement *--> GraphicPrototype

@enduml
